#include <string.h>

#include <glib.h>

#include <curl/curl.h>

#include "http.h"


const gchar* useragent
    = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
      "Chrome/87.0.4280.88 Safari/537.36";

struct buffer_t
{
    gchar* memory;
    gsize size;
};

static size_t request_callback(void* contents, size_t size, size_t nmemb,
                               void* userp)
{
    gsize realsize = size * nmemb;
    struct buffer_t* buffer = (struct buffer_t*)userp;

    gchar* ptr = g_realloc(buffer->memory, buffer->size + realsize + 1);
    g_assert(ptr);
    if(!ptr)
        return 0;

    buffer->memory = ptr;
    memcpy(&buffer->memory[buffer->size], contents, realsize);
    buffer->size += realsize;
    buffer->memory[buffer->size] = 0;
    return realsize;
}

gchar* do_request(const gchar* URL, gsize* page_size)
{
    g_assert(URL && *URL);

    CURL* curl = curl_easy_init();
    g_assert(curl);
    if(!curl)
        return NULL;

    struct buffer_t buffer = {NULL, 0};

    curl_easy_setopt(curl, CURLOPT_URL, URL);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, useragent);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, request_callback);

    CURLcode curl_result = curl_easy_perform(curl);
    g_assert(curl_result == CURLE_OK);
    if(curl_result != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        return NULL;
    }

    curl_easy_cleanup(curl);
    if(page_size)
        *page_size = buffer.size;
    return buffer.memory;
}
