/* GStreamer Vimeo plugin
 * Copyright (C) 2021 Andrew Kravchuk <awkravchuk@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-vimeosource
 *
 * The vimeosource element streams video from Vimeo hosting.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v vimeosource ! autovideosink
 * ]|
 * Plays given video.
 * </refsect2>
 */

#include <string.h>
#include <locale.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "http.h"
#include "vimeosource.h"


GST_DEBUG_CATEGORY_STATIC(_vimeosource_debug_category);
#define GST_CAT_DEFAULT _vimeosource_debug_category

/* prototypes */

static void _vimeosource_set_property(GObject* object, guint property_id,
                                      const GValue* value, GParamSpec* pspec);
static void _vimeosource_get_property(GObject* object, guint property_id,
                                      GValue* value, GParamSpec* pspec);
static void _vimeosource_finalize(GObject* object);

static gboolean _vimeosource_negotiate(GstBaseSrc* src);
static gboolean _vimeosource_start(GstBaseSrc* src);
static gboolean _vimeosource_stop(GstBaseSrc* src);
static gboolean _vimeosource_query(GstBaseSrc* src, GstQuery* query);
static GstFlowReturn _vimeosource_create(GstBaseSrc* src, guint64 offset,
                                         guint size, GstBuffer** buf);

static size_t curl_callback(void* contents, size_t size, size_t nmemb,
                            void* userp);

enum
{
    PROP_0,
    PROP_LOCATION
};

/* pad templates */

static GstStaticPadTemplate _vimeosource_src_template = GST_STATIC_PAD_TEMPLATE(
    "src", GST_PAD_SRC, GST_PAD_ALWAYS, GST_STATIC_CAPS("video/x-h264"));


/* class initialization */

G_DEFINE_TYPE_WITH_CODE(
    VimeoSource, _vimeosource, GST_TYPE_PUSH_SRC,
    GST_DEBUG_CATEGORY_INIT(_vimeosource_debug_category, "vimeosource", 0,
                            "debug category for vimeosource element"));

static void _vimeosource_class_init(VimeoSourceClass* klass)
{
    GObjectClass* gobject_class = G_OBJECT_CLASS(klass);
    GstBaseSrcClass* base_src_class = GST_BASE_SRC_CLASS(klass);

    /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
    gst_element_class_add_static_pad_template(GST_ELEMENT_CLASS(klass),
                                              &_vimeosource_src_template);

    gst_element_class_set_static_metadata(
        GST_ELEMENT_CLASS(klass), "Vimeo video source", "Generic",
        "Plays videos from Vimeo video hosting",
        "Andrew Kravchuk <awkravchuk@gmail.com>");

    gobject_class->set_property = _vimeosource_set_property;
    gobject_class->get_property = _vimeosource_get_property;
    gobject_class->finalize = _vimeosource_finalize;
    base_src_class->negotiate = GST_DEBUG_FUNCPTR(_vimeosource_negotiate);
    base_src_class->start = GST_DEBUG_FUNCPTR(_vimeosource_start);
    base_src_class->stop = GST_DEBUG_FUNCPTR(_vimeosource_stop);
    base_src_class->query = GST_DEBUG_FUNCPTR(_vimeosource_query);
    base_src_class->create = GST_DEBUG_FUNCPTR(_vimeosource_create);

    g_object_class_install_property(
        gobject_class, PROP_LOCATION,
        g_param_spec_string("location", "location", "URL of the video", "",
                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    curl_global_init(CURL_GLOBAL_DEFAULT);
}

static void _vimeosource_init(VimeoSource* vimeosource)
{
}

void _vimeosource_set_property(GObject* object, guint property_id,
                               const GValue* value, GParamSpec* pspec)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(object);

    GST_DEBUG_OBJECT(vimeosource, "set_property");

    switch(property_id)
    {
    case PROP_LOCATION:
        vimeosource->location = g_strdup(g_value_get_string(value));
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
        break;
    }
}

void _vimeosource_get_property(GObject* object, guint property_id,
                               GValue* value, GParamSpec* pspec)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(object);

    GST_DEBUG_OBJECT(vimeosource, "get_property");

    switch(property_id)
    {
    case PROP_LOCATION:
        g_value_set_string(value, vimeosource->location);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
        break;
    }
}

void _vimeosource_finalize(GObject* object)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(object);

    GST_DEBUG_OBJECT(vimeosource, "finalize");

    curl_global_cleanup();

    G_OBJECT_CLASS(_vimeosource_parent_class)->finalize(object);
}

/* decide on caps */
static gboolean _vimeosource_negotiate(GstBaseSrc* src)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(src);

    GST_DEBUG_OBJECT(vimeosource, "negotiate");

    return TRUE;
}

/* start and stop processing, ideal for opening/closing the resource */
static gboolean _vimeosource_start(GstBaseSrc* src)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(src);
    CURLMcode ret;

    GST_DEBUG_OBJECT(vimeosource, "start");

    setlocale(LC_NUMERIC, "C"); // see https://git.io/Jte2C
    vimeosource->file_location = get_file_url(vimeosource->location);
    setlocale(LC_NUMERIC, "");

    GST_DEBUG_OBJECT(vimeosource, "location=%s", vimeosource->file_location);

    vimeosource->curlm = curl_multi_init();
    g_assert(vimeosource->curlm);
    if(!vimeosource->curlm)
        return GST_FLOW_ERROR;

    vimeosource->curl = curl_easy_init();
    g_assert(vimeosource->curl);
    if(!vimeosource->curl)
        return GST_FLOW_ERROR;

    curl_easy_setopt(vimeosource->curl, CURLOPT_URL,
                     vimeosource->file_location);
    curl_easy_setopt(vimeosource->curl, CURLOPT_USERAGENT, useragent);
    curl_easy_setopt(vimeosource->curl, CURLOPT_WRITEDATA, src);
    curl_easy_setopt(vimeosource->curl, CURLOPT_WRITEFUNCTION, &curl_callback);

    ret = curl_multi_add_handle(vimeosource->curlm, vimeosource->curl);
    g_assert(ret == CURLM_OK);
    if(ret != CURLM_OK)
        return GST_FLOW_ERROR;

    int dummy;
    ret = curl_multi_perform(vimeosource->curlm, &dummy);
    g_assert(ret == CURLM_OK);
    if(ret != CURLM_OK)
        return GST_FLOW_ERROR;

    return TRUE;
}

static gboolean _vimeosource_stop(GstBaseSrc* src)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(src);

    GST_DEBUG_OBJECT(vimeosource, "stop");

    if(vimeosource->curlm)
    {
        CURLMcode ret
            = curl_multi_remove_handle(vimeosource->curlm, vimeosource->curl);
        g_assert(ret == CURLM_OK);
        if(ret != CURLM_OK)
            return FALSE;

        ret = curl_multi_cleanup(vimeosource->curlm);
        g_assert(ret == CURLM_OK);
        if(ret != CURLM_OK)
            return FALSE;

        vimeosource->curlm = NULL;
    }

    if(vimeosource->curl)
    {
        curl_easy_cleanup(vimeosource->curl);
        vimeosource->curl = NULL;
    }

    g_free(vimeosource->file_location);
    vimeosource->file_location = NULL;

    g_free(vimeosource->location);
    vimeosource->location = NULL;

    return TRUE;
}

/* notify subclasses of a query */
static gboolean _vimeosource_query(GstBaseSrc* src, GstQuery* query)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(src);
    gboolean ret = FALSE;

    GST_DEBUG_OBJECT(vimeosource, "query %s",
                     gst_query_type_get_name(GST_QUERY_TYPE(query)));

    switch(GST_QUERY_TYPE(query))
    {
    case GST_QUERY_URI:
        gst_query_set_uri(query, vimeosource->location);
        ret = TRUE;
        break;

    default:
        ret = FALSE;
        break;
    };

    if(!ret)
        ret = GST_BASE_SRC_CLASS(_vimeosource_parent_class)->query(src, query);

    return ret;
}

static size_t curl_callback(void* contents, size_t size, size_t nmemb,
                            void* userp)
{
    GstBaseSrc* src = userp;
    VimeoSource* vimeosource = _VIMEOSOURCE(src);
    gsize realsize = size * nmemb;

    // NOTE : also possible to use class GstAllocator

    vimeosource->current_buffer = gst_buffer_new();

    gchar* data = g_malloc(realsize);
    g_assert(data);
    if(!data)
        return 0;
    memcpy(data, contents, realsize);

    GstMemory* memory
        = gst_memory_new_wrapped(0, data, realsize, 0, realsize, data, g_free);
    g_assert(memory);
    if(!memory)
        return 0;

    gst_buffer_insert_memory(vimeosource->current_buffer, -1, memory);

    return realsize;
}

/* ask the subclass to create a buffer with offset and size, the default
 * implementation will call alloc and fill. */
static GstFlowReturn _vimeosource_create(GstBaseSrc* src, guint64 offset,
                                         guint size, GstBuffer** buf)
{
    VimeoSource* vimeosource = _VIMEOSOURCE(src);
    CURLMcode ret;

    GST_DEBUG_OBJECT(vimeosource, "create");

    vimeosource->current_buffer = NULL;

    while(!vimeosource->current_buffer)
    {
        gint numfds = 0;
        ret = curl_multi_poll(vimeosource->curlm, NULL, 0, 0, &numfds);
        g_assert(ret == CURLM_OK);
        if(ret != CURLM_OK)
            return GST_FLOW_ERROR;

        gint running;
        ret = curl_multi_perform(vimeosource->curlm, &running);
        g_assert(ret == CURLM_OK);
        if(ret != CURLM_OK)
            return GST_FLOW_ERROR;
        if(!running)
            break;
    }

    *buf = vimeosource->current_buffer;
    return GST_FLOW_OK;
}

static gboolean plugin_init(GstPlugin* plugin)
{
    return gst_element_register(plugin, "vimeosource", GST_RANK_NONE,
                                _TYPE_VIMEOSOURCE);
}

#ifndef VERSION
#define VERSION "0.0.1"
#endif
#ifndef PACKAGE
#define PACKAGE "vimeosource"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "vimeosource"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "https://gitlab.com/lockie/otus-video-player"
#endif

GST_PLUGIN_DEFINE(GST_VERSION_MAJOR, GST_VERSION_MINOR, vimeosource,
                  "Plays videos from Vimeo hosting", plugin_init, VERSION,
                  "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)
