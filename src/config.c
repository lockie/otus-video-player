#include <string.h>

#include <glib.h>

#include <libxml/HTMLparser.h>

#include <parson.h>

#include "http.h"


static const xmlChar* config_marker
    = BAD_CAST("window.vimeo.clip_page_config.player = ");

struct callback_data_t
{
    gboolean found;
    gsize allocated_size;
    xmlChar string[];
};

static void cdata_callback(void* ctx, const xmlChar* ch, int len)
{
    htmlParserCtxtPtr context = (htmlParserCtxtPtr)ctx;
    struct callback_data_t* data = context->_private;
    // NOTE : CDATA could be split in HTML_PARSER_BIG_BUFFER_SIZE size chunk,
    //  which is 1000 bytes
    if(data)
    {
        if(!data->found)
        {
            gsize new_chunk_length = 0;
            for(;
                ch[new_chunk_length] != ';' && &ch[new_chunk_length] < &ch[len];
                new_chunk_length++)
                ;

            data = context->_private
                = g_realloc(data,
                            sizeof(struct callback_data_t) + new_chunk_length
                                + data->allocated_size);
            memcpy(&data->string[data->allocated_size], ch, new_chunk_length);

            data->allocated_size += new_chunk_length;
            data->string[data->allocated_size] = 0;
            data->found = ch[new_chunk_length] == ';';
        }
    }
    else
    {
        const xmlChar* r = xmlStrstr(ch, config_marker);
        if(!r)
            return;
        const xmlChar* chunk = r + xmlStrlen(config_marker);

        gsize chunk_length = 0;
        for(; chunk[chunk_length] != ';' && &chunk[chunk_length] < &ch[len];
            chunk_length++)
            ;

        data = context->_private
            = g_malloc(sizeof(struct callback_data_t) + chunk_length);
        memcpy(data->string, chunk, chunk_length);
        data->string[chunk_length] = 0;
        data->allocated_size = chunk_length;
        data->found = chunk[chunk_length - 1] == ';';
    }
}

gchar* get_config_url(const gchar* video_url)
{
    gsize page_size;
    gchar* page = do_request(video_url, &page_size);
    g_assert(page);
    if(!page)
        return NULL;

    htmlParserCtxtPtr context = htmlCreateMemoryParserCtxt(page, page_size);
    int res = htmlCtxtUseOptions(
        context, XML_PARSE_RECOVER | XML_PARSE_NOERROR | XML_PARSE_NOBLANKS);
    g_assert(res == 0);
    if(res != 0)
    {
        g_free(page);
        return NULL;
    }
    context->sax->cdataBlock = cdata_callback;
    htmlParseDocument(context);

    xmlChar* config = ((struct callback_data_t*)context->_private)->string;
    g_assert(config);
    if(!config)
    {
        g_free(page);
        return NULL;
    }

    JSON_Value* config_json = json_parse_string((char*)config);
    g_assert(json_value_get_type(config_json) == JSONObject);
    if(json_value_get_type(config_json) != JSONObject)
    {
        g_free(context->_private);
        json_value_free(config_json);
        g_free(page);
        return NULL;
    }

    JSON_Object* config_object = json_value_get_object(config_json);
    gsize len = json_object_get_string_len(config_object, "config_url");
    g_assert(len != 0);
    if(len == 0)
    {
        g_free(context->_private);
        json_value_free(config_json);
        g_free(page);
        return NULL;
    }
    gchar* result = (gchar*)g_malloc(len + 1);
    strncpy(result, json_object_get_string(config_object, "config_url"),
            len + 1);

    g_free(context->_private);
    json_value_free(config_json);
    g_free(page);
    return result;
}

gchar* get_file_url(const gchar* video_url)
{
    gchar* config_url = get_config_url(video_url);
    if(!config_url)
        return NULL;

    char* page = do_request(config_url, NULL);
    g_assert(page);
    if(!page)
        return NULL;


    JSON_Value* config_json = json_parse_string(page);
    g_assert(json_value_get_type(config_json) == JSONObject);
    if(json_value_get_type(config_json) != JSONObject)
    {
        json_value_free(config_json);
        g_free(page);
        return NULL;
    }

    JSON_Object* config_object = json_value_get_object(config_json);
    JSON_Array* files
        = json_object_dotget_array(config_object, "request.files.progressive");
    g_assert(files);
    if(!files)
    {
        json_value_free(config_json);
        g_free(page);
        return NULL;
    }

    const gchar* URL = NULL;
    gsize URL_len = 0;
    gdouble width = 0;
    for(gsize i = 0; i < json_array_get_count(files); i++)
    {
        JSON_Object* file = json_array_get_object(files, i);
        const gchar* file_URL = json_object_get_string(file, "url");
        gdouble file_width = (gulong)json_object_get_number(file, "width");
        if(file_width > width)
        {
            width = file_width;
            URL = file_URL;
            URL_len = json_object_get_string_len(file, "url");
        }
    }
    g_assert(URL);
    if(!URL)
    {
        json_value_free(config_json);
        g_free(page);
        return NULL;
    }

    gchar* result = g_strndup(URL, URL_len);

    json_value_free(config_json);
    g_free(page);

    return result;
}
