#!/usr/bin/env sh

plugin_path=$(realpath "$(dirname "$0")")/bin
gst-launch-1.0 -v -m --gst-plugin-path="$plugin_path" \
               vimeosource location=https://vimeo.com/59785024 ! decodebin name=dmux \
               dmux. ! queue ! audioconvert ! autoaudiosink \
               dmux. ! queue ! autovideoconvert ! autovideosink
